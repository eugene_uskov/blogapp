//
//  Post.swift
//  Blog App
//
//  Created by Eugene Uskov on 12/17/17.
//  Copyright © 2017 Eugene Uskov. All rights reserved.
//

import Foundation
import FirebaseStorage
import FirebaseDatabase

// MARK: - Post

extension Post {
    fileprivate static let headerKey = "header"
    fileprivate static let textKey = "text"
    fileprivate static let authorNameKey = "authorName"
    fileprivate static let imageURLKey = "imageURL"
}

class Post {
    
    // MARK: - Properties
    
    var header = "test header"
    var text = "test text"
    var authorName = "test authorName"
    var imageUrl = "test img url"
    var ref: DatabaseReference?
    let key: String
    
    var dictionary: Any {
        return  [ // TODO: make string values as constants on top
            Post.headerKey: header,
            Post.textKey: text,
            Post.authorNameKey: authorName,
            Post.imageURLKey: imageUrl
        ]
    }
    
    // MARK: - Initializers
    
    init(header: String, text: String, authorName: String, imageUrl: String = "33", key: String = "", ref: DatabaseReference? = nil) {
        self.header = header
        self.text = text
        self.authorName = authorName
        self.imageUrl = imageUrl
        self.key = key
        self.ref = ref
    }
    
    convenience init(snapshot: DataSnapshot) {
        let key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        
        let header = snapshotValue[Post.headerKey] as! String
        let text = snapshotValue[Post.textKey] as! String
        let authorName = snapshotValue[Post.authorNameKey] as! String
        let imageURL = snapshotValue[Post.imageURLKey] as! String
        let ref = snapshot.ref
        self.init(header: header, text: text, authorName: authorName, imageUrl: imageURL, key: key, ref: ref)
    }
}
