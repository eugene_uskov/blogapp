//
//  DatabaseManager.swift
//  Blog App
//
//  Created by Eugene Uskov on 12/17/17.
//  Copyright © 2017 Eugene Uskov. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage


// MARK: DatabaseManager

class DatabaseManager {

    // MARK: - Properties
    
    static var ref = Database.database().reference(withPath: "posts")
    
    // MARK: - Methods
    
    static func save(post: Post) {
        self.ref.child(post.header).setValue(post.dictionary)
    }
}
