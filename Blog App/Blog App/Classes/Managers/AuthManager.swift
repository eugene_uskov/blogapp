//
//  AuthManager.swift
//  Blog App
//
//  Created by Eugene Uskov on 12/17/17.
//  Copyright © 2017 Eugene Uskov. All rights reserved.
//

import Foundation
import Firebase


typealias AuthSuccessCompletion = ((_ user: User) -> ())
typealias AuthFailureCompletion = ((_ error: Error) -> ())
typealias AuthChangeStateIsLoggedInCompletion = (() -> ())?
typealias AuthChangeStateIsNotLoggedInCompletion = (() -> ())?


// a few experiments here

// MARK: - AuthProvider

fileprivate protocol AuthProvider {
    var isUserLoggedIn: Bool { get }
    
    func login(withEmail email: String, password: String,
               success: @escaping AuthSuccessCompletion, failure: @escaping AuthFailureCompletion)
    
    func signUp(withEmail email: String, password: String,
                success: @escaping AuthSuccessCompletion, failure: @escaping AuthFailureCompletion)
    
    func addChangeStateListener(loggedInCompletion: AuthChangeStateIsLoggedInCompletion, notLoggedInCompletion: AuthChangeStateIsNotLoggedInCompletion);
    
    func logout()
}


// MARK: - AuthInstance

fileprivate class AuthInstance {
    
    // MARK: - Properties
    
    let auth = Auth.auth()
    var isUserLoggedIn: Bool { return auth.currentUser != nil }
    
    // MARK: - Initializers
    
    fileprivate init() {}
}


// MARK: - Methods

extension AuthInstance: AuthProvider {
    func login(withEmail email: String, password: String,
               success: @escaping AuthSuccessCompletion, failure: @escaping AuthFailureCompletion) {
        auth.signIn(withEmail: email, password: password) { user, error in
            // TODO: поменять на енам с саксесс и фeйлиэ
            guard error == nil, let user = user else {
                failure(error!);
                return;
            }
            success(user)
        }
    }
    
    func signUp(withEmail email: String, password: String,
                success: @escaping AuthSuccessCompletion, failure: @escaping AuthFailureCompletion) {
        auth.createUser(withEmail: email, password: password) { user, error in
            guard error == nil else {
                failure(error!);
                return;
            }
            self.login(withEmail: email, password: password, success: success, failure: failure)
        }
    }
    
    func addChangeStateListener(loggedInCompletion: AuthChangeStateIsLoggedInCompletion = nil,
                                notLoggedInCompletion: AuthChangeStateIsNotLoggedInCompletion = nil) {
        auth.addStateDidChangeListener { auth, user in
            if user == nil {
                notLoggedInCompletion?()
            } else {
                loggedInCompletion?()
            }
        }
    }
    
    func logout() {
        do {
            try auth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
}


// MARK: - AuthManager

class AuthManager {
    
    // MARK: - Properties
    
    private static var instance: AuthProvider = AuthInstance()
    
    
    // MARK: - Initializers
    
    private init() {}
}


// MARK: - Public Api

extension AuthManager {
    static var isUserLoggedIn: Bool { return self.instance.isUserLoggedIn  }
    
    static func login(withEmail email: String, password: String,
                      success: @escaping AuthSuccessCompletion, failure: @escaping AuthFailureCompletion) {
        self.instance.login(withEmail: email, password: password,
                            success: success, failure: failure)
    }
    
    static func signUp(withEmail email: String, password: String,
                       success: @escaping AuthSuccessCompletion, failure: @escaping AuthFailureCompletion) {
        self.instance.signUp(withEmail: email, password: password,
                             success: success, failure: failure)
    }
    
    static func addChangeStateListener(loggedInCompletion: AuthChangeStateIsLoggedInCompletion, notLoggedInCompletion: AuthChangeStateIsNotLoggedInCompletion) {
        self.instance.addChangeStateListener(loggedInCompletion: loggedInCompletion, notLoggedInCompletion: notLoggedInCompletion)
    }
    
    static func logout() {
        self.instance.logout()
    }
}






