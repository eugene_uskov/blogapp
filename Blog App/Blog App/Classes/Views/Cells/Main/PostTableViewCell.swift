//
//  PostTableViewCell.swift
//  Blog App
//
//  Created by Eugene Uskov on 12/17/17.
//  Copyright © 2017 Eugene Uskov. All rights reserved.
//

import UIKit


// MARK: - PostTableViewCell

class PostTableViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var imagView: UIImageView!
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    @IBOutlet weak var postLabel: UILabel!
    
    var post: Post! {
        didSet { configure() }
    }
}

// MARK: - Methods

extension PostTableViewCell {
    func configure() {
        headerLabel.text = post.header
        postLabel.text = post.text
        authorLabel.text = post.authorName
        
        if post.imageUrl != "" {
            imagView.image = UIImage(named: post.imageUrl)!
        } else {
            imagView?.image = #imageLiteral(resourceName: "avatar-template")
        }
    }
}

