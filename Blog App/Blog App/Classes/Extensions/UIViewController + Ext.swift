//
//  UIViewController + Ext.swift
//  Blog App
//
//  Created by Eugene Uskov on 12/17/17.
//  Copyright © 2017 Eugene Uskov. All rights reserved.
//

import UIKit
import SVProgressHUD

// MARK: Progress HUD Extension

extension UIViewController {
    func showProgressHUD() {
        SVProgressHUD.show()
    }
    
    func hideProgressHUD() {
        SVProgressHUD.dismiss()
    }
}

