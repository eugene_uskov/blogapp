//
//  LoginViewController.swift
//  Blog App
//
//  Created by Eugene Uskov on 12/17/17.
//  Copyright © 2017 Eugene Uskov. All rights reserved.
//


import UIKit


// MARK: LoginViewController

class LoginViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
}

// MARK: - Inner Types

fileprivate extension LoginViewController {
    
    struct Segues {
        private init() {}
        static let toMainStoryboard = "toMainStoryboard"
    }
}

// MARK: - Actions

fileprivate extension LoginViewController {
    @IBAction func loginButtonTapped(_ sender: Any) {
        login()
    }
}

// MARK: - Methods

fileprivate extension LoginViewController {
    func login() {
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        showProgressHUD()
        
        AuthManager.login(withEmail: email, password: password, success: { _ in
            self.hideProgressHUD()
            self.performSegue(withIdentifier: Segues.toMainStoryboard, sender: self)
        }) { _ in
            self.hideProgressHUD()
        }
    }
}




