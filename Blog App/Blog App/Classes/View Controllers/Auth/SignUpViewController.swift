//
//  SignUpViewController.swift
//  Blog App
//
//  Created by Eugene Uskov on 12/17/17.
//  Copyright © 2017 Eugene Uskov. All rights reserved.
//

import UIKit


// MARK: SignUpViewController

class SignUpViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
}

// MARK: - Inner Types

fileprivate extension SignUpViewController {
    struct Segues {
        private init() {}
        static let toMainStoryboard = "toMainStoryboard"
    }
}

// MARK: - Actions

fileprivate extension SignUpViewController {
    @IBAction func signUpButtonTapped(_ sender: Any) {
        signUp()
    }
}

// MARK: - Methods

fileprivate extension SignUpViewController {
    func signUp() {
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        self.showProgressHUD()
        
        AuthManager.signUp(withEmail: email, password: password, success: { _ in
            self.hideProgressHUD()
            self.performSegue(withIdentifier: Segues.toMainStoryboard, sender: self)
         }) { _ in
            self.hideProgressHUD()
        }
    }
}
