//
//  NewPostViewController.swift
//  Blog App
//
//  Created by Eugene Uskov on 12/17/17.
//  Copyright © 2017 Eugene Uskov. All rights reserved.
//

import UIKit
import FirebaseAuth

// MARK: - NewPostViewController

class NewPostViewController: UIViewController {

    // MARK: - Properties
    
    @IBOutlet weak var headerTextField: UITextField!
    @IBOutlet weak var textTextView: UITextView!
}

// MARK: - Actions

extension NewPostViewController {
    @IBAction func addTapped(sender: Any) {
        DatabaseManager.save(post: Post(header: headerTextField.text ?? "Empty header", text: textTextView.text, authorName: Auth.auth().currentUser?.email ?? "qq@qq.com"))
        dismiss(animated: true)
    }
    
    @IBAction func cancelTapped(sender: Any) {
        dismiss(animated: true)
    }
}

