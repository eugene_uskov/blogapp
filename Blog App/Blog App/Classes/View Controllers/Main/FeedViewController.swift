//
//  FeedViewController.swift
//  Blog App
//
//  Created by Eugene Uskov on 12/17/17.
//  Copyright © 2017 Eugene Uskov. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage


// MARK: - FeedViewController

class FeedViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!

    var posts: [Post] = []
    var searchedPosts: [Post]? = nil
    
    let cellIdentifier = "PostTableViewCell"
}

// MARK: - Inner Types

fileprivate extension FeedViewController {
    struct Segues {
        private init() {}
        static let toAuthScreens = "toAuthScreens"
    }
}

// MARK: - Lifecycle

extension FeedViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DatabaseManager.ref.observe(.value, with: { snapshot in
            self.posts = snapshot.children.map { return Post(snapshot: $0 as! DataSnapshot) }
            self.tableView.reloadData()
        })
        
        let gr = UITapGestureRecognizer(target: self, action: #selector(action))
        gr.numberOfTapsRequired = 2
        view.addGestureRecognizer(gr)
    }
}

// MARK: - Actions

extension FeedViewController {
    @IBAction func logoutButtonTapped() {
        AuthManager.logout()
        self.performSegue(withIdentifier: "toAuthScreens", sender: self)
    }
}

// MARK: - Methods

extension FeedViewController {
    @objc func action() {
        self.searchBar.resignFirstResponder()
    }
}

// MARK: - UITableViewDataSource

extension FeedViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedPosts != nil ? searchedPosts!.count : posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! PostTableViewCell
        cell.post = searchedPosts != nil ? searchedPosts![indexPath.row] : posts[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate

extension FeedViewController: UITableViewDelegate {
    
}

// MARK: - UISearchBarDelegate

extension FeedViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            searchedPosts = nil
            tableView.reloadData()
        } else {
            searchedPosts = posts.filter { $0.authorName.contains(searchText) }
            tableView.reloadData()
        }
    }
}


